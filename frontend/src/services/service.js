import axios from 'axios';

export function query(req) {
  let token = localStorage.getItem('tokenEuroPrint');
  const path = req.path;
  delete req.path;

  return axios({
    url: 'http://localhost' + path,
    method: "get",
    params: req,
    headers:{
      Authorization:"Bearer "+token
    }
  })
}

export function create(req) {
  let token = localStorage.getItem('tokenEuroPrint');
  const path = req.path;
  delete req.path;
  delete req.remember;

  return axios({
    url: 'http://localhost' + path,
    method: "post",
    data: req,
    headers:{
      Authorization:"Bearer "+token
    }
  })
}

export function remove(req) {

  let token = localStorage.getItem('tokenEuroPrint');
  // const path = req.path;
  // delete req.path;
  return axios({
    url: 'http://localhost' + req,
    method: "delete",
    // params: req,
    headers:{
      Authorization:"Bearer "+token
    }
  })
}

export function edit(req) {
  let token = localStorage.getItem('tokenEuroPrint');
  const path = req.path;
  delete req.path;

  return axios({
    url: 'http://localhost' + path,
    method: "patch",
    data: req,
    headers:{
      Authorization:"Bearer "+token
    }
  })
}
