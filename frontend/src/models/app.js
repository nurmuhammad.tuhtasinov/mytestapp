import {query} from "../services/service";
import router from "umi/router";

export default {

  namespace:'app',
  state:{
    user:'',
  },
  subscriptions:{

    setup({dispatch, history}) {

      if (history.location.pathname!=='/signUp'){
        dispatch({
          type:'query',
          payload:{
            path:'/api/user/me'
          }
        })
      }

    }

  },
  effects:{
    *query({payload},{call,put,select}){
      try {
        const {data} = yield call(query,payload);
        yield put({
          type:'updateState',
          payload:{
            user:data
          }
        })
      }catch (e) {
        router.push("/login")
      }
    }
  },
  reducers:{
    updateState(state, {payload}) {
      return {
        ...state,
        ...payload,
      }
    },
  }

}
