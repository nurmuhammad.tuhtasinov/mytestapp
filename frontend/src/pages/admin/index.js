import React, {Component} from 'react';
import {Button, Card,  Col, Form, Input, Modal, Radio, Row} from "antd";
import {connect} from 'dva';
import { Popconfirm } from 'antd';


@connect(({adminModel})=>({adminModel}))
class Index extends Component {
  render() {

    const {dispatch,adminModel,form}=this.props;
    const {data,data2,modalVisible,current,radioSelect}=adminModel;
    const {getFieldDecorator,resetFields,validateFields}=form;

    const onLogOut=()=>{
      localStorage.clear();
      window.location.reload();
    };

    const onUserDelete=(id)=>{
      dispatch({
        type:'adminModel/remove',
        payload: '/api/user/'+id
      })
    };

    const onAnswerDelete=(id)=>{
      dispatch({
        type:'adminModel/remove',
        payload: '/api/question/'+id,
      })
    };

    const onAnswerEdit=(id)=>{
      dispatch({
        type:'adminModel/currentQuestion',
        payload:{
          path:'/api/question/current',
          id:id
        }
      })
      onModalOpen();
    };

    const onModalOpen=()=>{
      dispatch({
        type:'adminModel/updateState',
        payload: {
          modalVisible:true,
        }
      })
    };

    const onModalHide=()=>{
      dispatch({
        type:'adminModel/updateState',
        payload: {
          modalVisible:false,
          current:'',
          radioSelect:'a',
        }
      })
      resetFields()
    };

    const onPress=()=>{
      validateFields((errors,values)=>{
        if(!errors){
          if (current){
            dispatch({
              type:'adminModel/updateQuestion',
              payload:{
                ...values,
                path:'/api/question',
                id:current.id,
                answer:radioSelect
              }
            })
          }else {
            dispatch({
              type:'adminModel/creatQuestion',
              payload:{...values,path:'/api/question', answer:radioSelect}
            })
          }
          onModalHide()
        }
      })
    };

    const onChangRadio=(event)=>{
      dispatch({
        type:'adminModel/updateState',
        payload: {
         radioSelect: event.target.value,
        }
      })
    };



    return (
      <div>
        <Modal onOk={onPress} title={current? "Edit question":"Add question"} onCancel={onModalHide} visible={modalVisible}>
          <Form>
            <Form.Item>
              {getFieldDecorator('question',{initialValue: current&&current.description,
                rules:[{required:true,message:'Input question'}]
              })(<Input placeholder={"Question..."}/>)}
            </Form.Item>

            <Row>
              <Col span={1} offset={2}>
                <Radio.Group defaultValue={"a"} onChange={onChangRadio} >
                  <Radio  value={'a'}></Radio><br/><br/><br/>
                  <Radio value={'b'}></Radio><br/><br/><br/>
                  <Radio value={'c'}></Radio><br/><br/><br/>
                  <Radio value={'d'}></Radio>
                </Radio.Group>

              </Col>

              <Col span={18} offset={1}>
                <Form.Item>
                  {getFieldDecorator('answer1',{ initialValue: current&&current.answers[0].description,
                    rules:[{required:true,message:'Input  answer 1 '}]
                  })(<Input placeholder={"Answer 1 ..."}/>)}
                </Form.Item>
                <Form.Item>
                  {getFieldDecorator('answer2',{initialValue: current&&current.answers[1].description,
                    rules:[{required:true,message:'Input answer 2'}]
                  })(<Input placeholder={"Answer 2..."}/>)}
                </Form.Item>
                <Form.Item>
                  {getFieldDecorator('answer3',{initialValue: current&&current.answers[2].description,
                    rules:[{required:true,message:'Input answer 3'}]
                  })(<Input placeholder={"Answer 3..."}/>)}
                </Form.Item>
                <Form.Item>
                  {getFieldDecorator('answer4',{initialValue: current&&current.answers[3].description,
                    rules:[{required:true,message:'Input answer 4'}]
                  })(<Input placeholder={"Answer 4..."}/>)}
                </Form.Item>
              </Col>
            </Row>
          </Form>
        </Modal>
        <Row>
          <Col span={9} offset={1}>
            <h3>Users List</h3>
          </Col>
          <Col  span={10} offset={1}>
            <h3 style={{float:"left"}}>Questions List</h3>
            <Button  onClick={onModalOpen} shape={"round"} style={{"backgroundColor":"#858B86", "color":"white",float:"right"}}>+ add question</Button>
          </Col>
          <Col span={2} offset={1}>
            <h6>{"Hello, Admin"}</h6>
            <Button onClick={onLogOut} type={"danger"} icon={"logout"}></Button>

          </Col>
        </Row>

        <Row>
          <Col span={9} offset={1} >
            {data.map((item,index)=>
              <Card >
                <h5 style={{"float":"left"}}>{item && index+1+'.  '+item.firstName+' '+item.lastName}</h5>
                <Popconfirm onConfirm={()=>onUserDelete(item.id)} title="Are you sure？" okText="Yes" cancelText="No">
                  <Button  style={{"float":"right"}} type={"danger"} icon={"delete"}></Button>
                </Popconfirm>
              </Card>)}
          </Col>

          <Col span={10} offset={1}>
            {data2.map((item,index)=>
              <Card >
                <Row>
                  <h5 style={{"float":"left"}}>{item && index+1+'.  '+item.description}</h5>
                  <Popconfirm onConfirm={()=>onAnswerDelete(item.id)} title="Are you sure？" okText="Yes" cancelText="No">
                    <Button  style={{"float":"right"}} type={"danger"} icon={"delete"}></Button>
                  </Popconfirm>
                  <Button onClick={()=>onAnswerEdit(item.id)} style={{"float":"right"}} type={"primary"} icon={"edit"}></Button>
                </Row>
                {item && item.answers.map(sItem=><p>{sItem.description}</p>)}
              </Card>)}
          </Col>

        </Row>
      </div>
    );
  }
}

export default Form.create()(Index);
