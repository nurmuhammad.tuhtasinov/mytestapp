import {query, create,remove,edit} from '../../services/service'
import {notification} from "antd";


export default {

  namespace:"adminModel",

  state:{
    data:[],
    data2:[],
    modalVisible:false,
    current:'',
    radioSelect:'a',
  },

  subscriptions:{
    setup({dispatch, history}){
      dispatch({
        type:'query',
        payload:{
          path:'/api/admin',
        }
      })
    }
  },

  effects:{
    * query({payload}, {put, call, select}) {
      const {data} = yield call(query, payload);
      if (data.success){
        yield put({
          type: 'updateState',
          payload: {
            data: data.object,
            data2: data.object2,
          }
        })
      }
    },

    * remove({payload}, {put, call, select}) {
      try {
        const {data} = yield call(remove, payload);
        notification.success({message:"Successfully deleted"})
        yield put({
          type: 'query',
          payload: {
            path:'/api/admin'
          }
        })
      }catch (e) {
        alert("You don't have access")
      }
    },

    *creatQuestion({payload}, {put, call, select}){
      try {
        const {data} = yield call(create, payload);
        yield put({
          type: 'query',
          payload: {
            path:'/api/admin'
          }
        })
      }catch (e) {
        alert("You don't have access")
      }
    },

    *currentQuestion({payload},{put,call,select}){
        const {data}=yield call(query,payload);
        if (data.success){
          yield put({
            type: 'updateState',
            payload:{
              current: data.object
            }
          })
        }
    },

    *updateQuestion({payload},{put,call,select}){
      try {
        const {data}=yield call(edit,payload)
        if(data.success){
          notification.success({message:"Successfully updated"})
          yield put({
            type: 'query',
            payload: {
              path:'/api/admin'
            }
          })
        }
      }catch (e) {
        alert("You don't have access")
      }
    }
  },


  reducers:{
    updateState(state, {payload}) {
      return {
        ...state,
        ...payload,
      }
    },
  }

}
