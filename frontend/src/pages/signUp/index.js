import React, {Component} from 'react';
import {Button, Card, Col, Form, Input, Row} from "antd";
import {connect} from 'dva';
import router from "umi/router";

@connect(({signUpModel})=>({signUpModel}))
class Index extends Component {
  render() {
    const {signUpModel,dispatch,form}=this.props;
    const {data}=signUpModel;
    const {getFieldDecorator,validateFields}=form;

    const onRegister=()=>{
      validateFields((errors,values)=>{
        if(!errors){
          dispatch({
            type:'signUpModel/registerUser',
            payload:{...values,path:'/api/auth/register'}
          })
        }
      })
    };

    const onEdit=()=>{
      validateFields((errors,values)=>{
        if(!errors){
          dispatch({
            type:'signUpModel/update',
            payload:{...values,path:'/api/user'}
          })
        }
      })
    };

    const onCancel=()=>{
      router.push("/test");
      // window.location.reload();
    }

    const onBack=()=>{
      router.push("/login");
      // window.location.reload();
    }

    return (
      <Row>
        <Col span={10} offset={7}>
          <Card>

            <Form>
              <Form.Item>
                {getFieldDecorator('userName',{
                  rules:[
                    {required:true,message:"Username required"}
                  ],initialValue: data&&data.username
                })(<Input  placeholder={"Username..."}/>)}<span hidden={true}>Bunday user bor</span>
              </Form.Item>
              <Form.Item>
                {getFieldDecorator('password',{
                  rules:[
                    {required:true, message:"Password required"}
                  ],initialValue: data&&data.password
                })(<Input  placeholder={"Password..."}/>)}
              </Form.Item>
              <Form.Item>
                {getFieldDecorator('firstName',{
                  rules:[
                    {required:true,message:"First name required"}
                  ],initialValue: data&&data.firstName
                })(<Input placeholder={"First name..."}/>)}
              </Form.Item>
              <Form.Item>
                {getFieldDecorator('lastName',{
                  rules:[
                    {required:true,message:"Last name required"}
                  ],initialValue: data&&data.lastName
                })(<Input placeholder={"Last name..."}/>)}
              </Form.Item>
              <Form.Item>
                {getFieldDecorator('patron',{
                  rules:[
                    {required:false}
                  ],initialValue: data&&data.patron
                })(<Input placeholder={"Father name..."}/>)}
              </Form.Item>
              <Form.Item>
                {getFieldDecorator('phoneNumber',{
                  rules:[
                    {required:true,message:"Phone number required"}
                  ],initialValue: data&&data.phoneNumber
                })(<Input placeholder={"Phone number name..."}/>)}
              </Form.Item>
              <Form.Item>
                {getFieldDecorator('email',{
                  rules:[
                    {required:false}
                  ],initialValue: data&&data.email
                })(<Input placeholder={"email..."}/>)}
              </Form.Item>
              {
                data?
                  <div>
                    <Button  onClick={onCancel} style={{"float":"right"}} type="danger" shape="round"  size={'large'}>Cancel</Button>
                    <Button onClick={onEdit} style={{"float":"right"}} type="primary" shape="round"  size={'large'}>Update</Button>
                  </div>
                  :
                  <div>
                    <Button  onClick={onBack} style={{"float":"right"}} type="danger" shape="round"  size={'large'}>Cancel</Button>
                    <Button onClick={onRegister} style={{"float":"right"}} type="primary" shape="round" icon="register" size={'large'}>Register</Button>
                  </div>
              }

            </Form>
          </Card>

        </Col>
      </Row>


    );
  }
}

export default Form.create()(Index);
