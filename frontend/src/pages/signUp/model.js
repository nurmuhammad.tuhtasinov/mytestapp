import {query, create,edit} from '../../services/service'
import router from "umi/router";
import {notification} from "antd";

export default {

  namespace:"signUpModel",

  state:{
    data:'',
  },

  subscriptions:{
    setup({dispatch, history}){
      dispatch({
        type:'query',
        payload:{
          path:'/api/auth/me',
        }
      })
    }
  },

  effects:{
    * query({payload}, {put, call, select}) {
      const {data} = yield call(query, payload);
      if (data.success){
        yield put({
          type: 'updateState',
          payload: {
            data: data.object,
          }
        })
      }
    },

    * registerUser({payload}, {put, call, select}) {
      try {
        const {data} = yield call(create, payload);
        notification.success({message:"successfully registered"})
        router.push("/login")
      }catch (e) {
        alert("This user exists")
      }
    },

    * update({payload}, {call, put, select}) {
      const {data} = yield call(edit, payload);
      console.log(data)
      if (data.success){
        router.push("/test");
        window.location.reload();
      }
    }
  },

  reducers:{
    updateState(state, {payload}) {
      return {
        ...state,
        ...payload,
      }
    },
  }

}
