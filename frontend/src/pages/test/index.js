import React, {Component} from 'react';
import {connect} from 'dva';
import {Button, Col, Radio, Row} from 'antd';
import router from "umi/router";

@connect(({testModel,app,signUpModel})=>({testModel,app,signUpModel}))
class Index extends Component {

  render() {
    const {testModel,dispatch,app}=this.props;
    const {data,result,questionNumber}=testModel;
    const {user}=app;

    const  onRadioChange = (event) => {
      dispatch({
        type: 'testModel/checkAnswerCount',
        payload: {
          path: "/api/checkAnswer",
          id: event.target.value
        }
      })
    };

    const onLogOut=()=>{
      localStorage.clear();
      window.location.reload();
    };

    const onEdit=()=>{
     router.push("/signUp")
    };

    const reBegin=()=>{
      dispatch({
        type:'testModel/updateState',
        payload:{
         result:''
        }
      });
      dispatch({
        type:'testModel/query',
        payload:{
          path:'/api/question',
          newPage:1,
        }
      })
    };



    return (
      <Row>
        {result=='' ?
          <Col span={10} offset={7}>
            <h4>
              {questionNumber&&questionNumber+". "}
              { data.length!==0 &&  data[0].description }
            </h4>
            <Radio.Group onChange={onRadioChange} >
              { data.length!==0 && data[0].answers.map(item=> <div><Radio className={"mt-2 "} value={item.id}>{item.description}</Radio><br/></div>)}
            </Radio.Group>
          </Col>:
        <Col span={10} offset={7}>
          <h4>Sizning toplagan balingiz:</h4>
          <h2>{result}</h2>
          <Button onClick={reBegin} type={"primary"}>Qaytadan boshlash</Button>
        </Col>}
        <Col span={3} offset={4}>
          <h6>{"Hello, "+user.firstName}</h6>
          <Button onClick={onEdit} type={"primary"} icon={"edit"}></Button>
          <Button onClick={onLogOut} type={"danger"} icon={"logout"}></Button>
        </Col>
      </Row>




    );
  }
}

export default Index;
