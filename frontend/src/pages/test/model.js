import {create, edit, query} from "../../services/service";
import {notification} from "antd";

export default {

  namespace:'testModel',

  state:{
    data:[],
    result:'',
    questionNumber:'',


  },

  subscriptions:{
    setup({dispatch, history}){
      dispatch({
        type:'query',
        payload:{
          path:'/api/question',
          newPage:1,
        }
      })
    }

  },

  effects: {
    * query({payload}, {put, call, select}) {
      const {data} = yield call(query, payload);
      yield put({
        type: 'updateState',
        payload: {
          data: data.object.content,
          questionNumber: data.object.number+1,
        }
      })
    },

    *checkAnswerCount({payload}, {put, call, select}) {
      const {data} = yield call(query, payload);
      if (data.success){
        if(data.message==="success"){
          yield put({
            type: 'updateState',
            payload: {
              data: data.object.content,
              questionNumber: data.object.number+1,
            }
          })
        }else {
          notification.success({message:"Successfully finished"})
          yield put({
            type: 'updateState',
            payload: {
              result: data.object
            }
          })
        }
      }
    },
  },


  reducers:{
    updateState(state,{payload}){
      return{
        ...state,
        ...payload
      }
    }

  }
}
