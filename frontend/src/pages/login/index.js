import React, {Component} from 'react';
import {Form, Icon, Input, Button, Checkbox, Row, Col,} from 'antd';
import {connect} from 'dva';



@connect(({login}) => ({login}))
class Index extends Component {


  render() {

    const {dispatch, login, form} = this.props;
    const {} = login;
    const { getFieldDecorator,validateFields } = form;

    const onLogin=()=>{
      validateFields((errors,values)=>{
        if(!errors){
          dispatch({
            type:'login/signIn',
            payload:{...values,path:'/api/auth/login'}
          })
        }
      })
    }

    return (

      <Row>
        <Col span={6} offset={9}>
          <Form onSubmit={this.handleSubmit} >
            <Form.Item>
              {getFieldDecorator('userName', {
                rules: [{ required: true, message: 'Please input your username!' }],
              })(
                <Input
                  prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
                  placeholder="Username"
                />,
              )}
            </Form.Item>
            <Form.Item>
              {getFieldDecorator('password', {
                rules: [{ required: true, message: 'Please input your password!' }],
              })(
                <Input
                  prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
                  type="password"
                  placeholder="Password"
                />,
              )}
            </Form.Item>
            <Form.Item>
              {getFieldDecorator('remember', {
                valuePropName: 'checked',
                initialValue: true,
              })(<Checkbox style={{"float": "left"}} >Remember me</Checkbox>)}
              <a style={{"float": "right"}} href="">
                Forgot password
              </a>
              <Button onClick={onLogin} type="primary" htmlType="submit"  style={{"width": "100%"}}>
                Log in
              </Button>
              Or <a href="/signUp">register now!</a>
            </Form.Item>
          </Form>
        </Col>
      </Row>
    );
  }
}

export default Form.create()(Index);



