import {create, query} from '../../services/service';
import router from "umi/router";


export default {

  namespace:"login",
  state:{
    user:'',

  },
  subscriptions:{

  },
  effects:{
    * query({payload}, {put, call, select}) {
      const {data} = yield call(query, payload);

      if (data.object.roles[0].name==='ROLE_USER'){
        router.push("/test")
      }else {
        router.push("/admin")
      }
      window.location.reload();
    },

    *signIn({payload},{call,put,select}){
      try {
        const {data} =yield call(create,payload);
        localStorage.setItem("tokenEuroPrint",data.accessToken);
        yield put({
          type: 'query',
          payload: {
            path:'/api/auth/me',
          }
        })
      }catch (e) {
        alert("Username or Password is invalid!!!")
      }
    }
  },

  reducers:{
    updateState(state, {payload}) {
      return {
        ...state,
        ...payload,
      }
    },
  }

}
