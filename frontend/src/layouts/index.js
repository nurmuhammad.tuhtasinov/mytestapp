import styles from './index.css';
import 'bootstrap/dist/css/bootstrap.min.css'
import  App from './app'


function BasicLayout(props) {
  return (
    <div className={styles.normal}>
      <h1 className={styles.title}> Welcome to MyApp!</h1>
      <App children={props.children}/>
    </div>
  );
}

export default BasicLayout;
