package ecma.demo.apptest.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor

public class ReqUser {

    private String userName;
    private String password;
    private String firstName;
    private String lastName;
    private String patron;
    private String phoneNumber;
    private String email;


    public ReqUser(String userName, String password, String firstName, String lastName, String phoneNumber) {
        this.userName = userName;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.phoneNumber = phoneNumber;
    }
}
