package ecma.demo.apptest.config;


import ecma.demo.apptest.entity.Role;
import ecma.demo.apptest.entity.User;
import ecma.demo.apptest.entity.enums.RoleName;
import ecma.demo.apptest.repository.RoleRepository;
import ecma.demo.apptest.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Component
public class DataLoader implements CommandLineRunner {

    @Autowired
    UserRepository userRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    RoleRepository roleRepository;

    @Override
        public void run(String... args) throws Exception {
//        List<Role> roles = new ArrayList<>();
//        roles.add(new Role(1, RoleName.ROLE_USER, "foydalanuvchi"));
//        roles.add(new Role(2, RoleName.ROLE_ADMIN, "Admin"));
//        roleRepository.saveAll(roles);
//        List<Role> adminRole = new ArrayList<>();
//        adminRole.add(new Role(2, RoleName.ROLE_ADMIN, "Admin"));
//        Set<Role> roles1 = new HashSet<>(adminRole);
//        userRepository.save(new User(
//                "Eagle4488",
//                passwordEncoder.encode("root123"),
//                "Nurmuhammad",
//                "Tuhtasinov",
//                "Baxodirovich",
//                "+998995119971",
//                roles1
//        ));
    }
}
