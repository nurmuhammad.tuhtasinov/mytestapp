package ecma.demo.apptest.entity.enums;

public enum RoleName {

    ROLE_ADMIN,
    ROLE_USER

}
