package ecma.demo.apptest.entity;


import com.fasterxml.jackson.annotation.JsonIgnore;
import ecma.demo.apptest.entity.template.AutoUuid;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "Answers")
public class Answer extends AutoUuid {

//    @Id
//    @GeneratedValue()
//    private Integer id;

    @Column(nullable = false)
    private String description;

    @JsonIgnore
    @Column(nullable = false)
    private Boolean status;

}
