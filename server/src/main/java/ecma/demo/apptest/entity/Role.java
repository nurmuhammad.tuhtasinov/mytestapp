package ecma.demo.apptest.entity;


import ecma.demo.apptest.entity.enums.RoleName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "Role")
public class Role implements GrantedAuthority {

    @Id
    private int id;

    @Basic
    @Enumerated(EnumType.STRING)
    @Column(name = "name", nullable = false)
    private RoleName name;

    private String description;

    @Override
    public String getAuthority() {
        return this.name.name();
    }
}
