package ecma.demo.apptest.entity;

import ecma.demo.apptest.entity.template.AutoUuid;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity

public class CheckAnswer extends AutoUuid {

    private UUID userId;

    private Integer count;

    private Double totalMark;

    private Integer page;

    public CheckAnswer(UUID userId, Integer count) {
        this.userId = userId;
        this.count = count;
    }
}
