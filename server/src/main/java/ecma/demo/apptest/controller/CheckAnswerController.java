package ecma.demo.apptest.controller;

import com.sun.deploy.net.HttpResponse;
import ecma.demo.apptest.entity.CheckAnswer;
import ecma.demo.apptest.entity.User;
import ecma.demo.apptest.payload.ApiResponse;
import ecma.demo.apptest.repository.AnswerRepository;
import ecma.demo.apptest.repository.CheckAnswerRepository;
import ecma.demo.apptest.repository.QuestionRepository;
import ecma.demo.apptest.security.CurrentUser;
import ecma.demo.apptest.service.QuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping("api/checkAnswer")
public class CheckAnswerController {

    @Autowired
    AnswerRepository answerRepository;
    @Autowired
    QuestionService questionService;
    @Autowired
    CheckAnswerRepository checkAnswerRepository;
    @Autowired
    QuestionRepository questionRepository;




    @GetMapping
    public HttpEntity<?> checkAnswer (@RequestParam UUID id, @CurrentUser User user){

        Integer allQuestionCount = questionRepository.allQuestionCount();
        int markPerQuestion=100/allQuestionCount;
        int count=0;
        int page=1;
        if (!answerRepository.findById(id).get().getStatus().equals(true)){
            count+=1;
            if (checkAnswerRepository.findByUserId(user.getId()).isPresent()){
                CheckAnswer havingCheckAnswer = checkAnswerRepository.findByUserId(user.getId()).get();
                havingCheckAnswer.setCount(havingCheckAnswer.getCount()+count);
                checkAnswerRepository.save(havingCheckAnswer);
            }else{
                checkAnswerRepository.save(new CheckAnswer(user.getId(), count,0d,page));
            }
        }
        if (answerRepository.findById(id).get().getStatus().equals(true)){
            count+=1;
            if (checkAnswerRepository.findByUserId(user.getId()).isPresent()){
                CheckAnswer havingCheckAnswer = checkAnswerRepository.findByUserId(user.getId()).get();
                havingCheckAnswer.setCount(havingCheckAnswer.getCount()+count);
                checkAnswerRepository.save(havingCheckAnswer);
            }else{
                checkAnswerRepository.save(new CheckAnswer(user.getId(), count,0d,page));
            }
            Double markPerAnswer=(double)markPerQuestion/(double)checkAnswerRepository.findByUserId(user.getId()).get().getCount();
            CheckAnswer readyCheckAnswer = checkAnswerRepository.findByUserId(user.getId()).get();
            readyCheckAnswer.setCount(0);
            readyCheckAnswer.setTotalMark(readyCheckAnswer.getTotalMark()+markPerAnswer);
            readyCheckAnswer.setPage(readyCheckAnswer.getPage()+1);
            CheckAnswer save2 = checkAnswerRepository.save(readyCheckAnswer);
            return questionService.getAllQuestions(user,save2.getPage());
        }
        return null;
    }
}
