package ecma.demo.apptest.controller;

import ecma.demo.apptest.entity.User;
import ecma.demo.apptest.payload.ReqQuestion;
import ecma.demo.apptest.security.CurrentUser;
import ecma.demo.apptest.service.QuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("api/question")
public class QuestionController {

    @Autowired
    QuestionService questionService;

    @GetMapping
    public HttpEntity<?> getAllQuestions (@CurrentUser User user, @RequestParam Integer newPage){
        return questionService.getAllQuestions(user,newPage);
    }

    @PostMapping
    public HttpEntity<?> saveQuestion (@RequestBody ReqQuestion reqQuestion){
        return questionService.saveQuestion(reqQuestion);
    }

    @DeleteMapping("{id}")
    public HttpEntity<?> deleteQuestion(@PathVariable UUID id){
        return questionService.deleteQuestion(id);
    }

    @GetMapping("current")
    public HttpEntity<?> getCurrentQuestion(@RequestParam UUID id){
        return questionService.getCurrentQuestion(id);
    }

    @PatchMapping
    public HttpEntity<?> updateQuestion(@RequestBody ReqQuestion reqQuestion){
        return questionService.updateQuestion(reqQuestion);
    }


}
