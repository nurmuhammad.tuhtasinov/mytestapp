package ecma.demo.apptest.controller;


import ecma.demo.apptest.entity.User;
import ecma.demo.apptest.payload.ApiResponse;
import ecma.demo.apptest.payload.JwtAuthenticationResponse;
import ecma.demo.apptest.payload.ReqSignIn;
import ecma.demo.apptest.payload.ReqUser;
import ecma.demo.apptest.repository.UserRepository;
import ecma.demo.apptest.security.CurrentUser;
import ecma.demo.apptest.security.JwtTokenProvider;
import ecma.demo.apptest.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/auth")
public class
AuthController {

    @Autowired
    JwtTokenProvider tokenProvider;

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    UserRepository userRepository;
    @Autowired
    PasswordEncoder passwordEncoder;
    @Autowired
    UserService userService;

    @PostMapping("/login")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody ReqSignIn reqSignIn) {
        Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(reqSignIn.getUserName(), reqSignIn.getPassword()));
        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = tokenProvider.generateToken(authentication);
        return ResponseEntity.ok(new JwtAuthenticationResponse(jwt));
    }

    @PostMapping("/register")
    public HttpEntity<?> saveUser(@RequestBody ReqUser reqUser){
        return userService.saveUser(reqUser);
    }

    @GetMapping("/me")
    public HttpEntity<?> getCurrentUser(@CurrentUser User user){
        return ResponseEntity.ok(new ApiResponse("success",true,user));
    }




}

