package ecma.demo.apptest.controller;


import ecma.demo.apptest.entity.Question;
import ecma.demo.apptest.entity.User;
import ecma.demo.apptest.payload.ApiResponse;
import ecma.demo.apptest.repository.QuestionRepository;
import ecma.demo.apptest.repository.UserRepository;
import ecma.demo.apptest.security.CurrentUser;
import ecma.demo.apptest.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.awt.print.Pageable;
import java.util.List;

@RestController
@RequestMapping("/api/admin")
public class AdminController {

    @Autowired
    UserRepository userRepository;
    @Autowired
    QuestionRepository questionRepository;


    @GetMapping
    public HttpEntity<?> getAllData(){
        List<User> allUser = userRepository.findAll();
        List<Question> allQuestion = questionRepository.findAllByOrderByDescription();
        return ResponseEntity.ok(new ApiResponse("success",true,allUser,allQuestion));
    }
}
