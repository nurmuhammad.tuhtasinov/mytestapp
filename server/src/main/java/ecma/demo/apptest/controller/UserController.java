package ecma.demo.apptest.controller;



import ecma.demo.apptest.entity.User;

import ecma.demo.apptest.payload.ReqUser;
import ecma.demo.apptest.repository.UserRepository;
import ecma.demo.apptest.security.CurrentUser;
import ecma.demo.apptest.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/api/user")
public class UserController {

    @Autowired
    UserService userService;


    @GetMapping("me")
    public HttpEntity<?> getCurrentUser(@CurrentUser User user){
         return ResponseEntity.ok(user);
    }

    @PatchMapping
    public HttpEntity<?> updateUser(@CurrentUser User user, @RequestBody ReqUser reqUser){
        return userService.updateUser(user,reqUser);
    }

    @DeleteMapping("{id}")
    public HttpEntity<?> deleteUser (@PathVariable UUID id){
        return userService.deleteUser(id);
    }





}
