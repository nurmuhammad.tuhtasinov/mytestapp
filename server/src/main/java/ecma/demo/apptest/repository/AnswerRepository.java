package ecma.demo.apptest.repository;

import ecma.demo.apptest.entity.Answer;
import ecma.demo.apptest.entity.Question;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface AnswerRepository extends JpaRepository<Answer, UUID> {

}
