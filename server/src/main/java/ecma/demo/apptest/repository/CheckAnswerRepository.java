package ecma.demo.apptest.repository;

import ecma.demo.apptest.entity.Answer;
import ecma.demo.apptest.entity.CheckAnswer;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface CheckAnswerRepository extends JpaRepository<CheckAnswer, UUID> {

    Optional<CheckAnswer> findByUserId (UUID id);

}
