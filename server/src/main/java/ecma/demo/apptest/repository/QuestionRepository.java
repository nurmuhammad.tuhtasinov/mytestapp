package ecma.demo.apptest.repository;

import ecma.demo.apptest.entity.Question;
import ecma.demo.apptest.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface QuestionRepository extends JpaRepository<Question, UUID> {

    @Query(value = "select count(*) from questions",nativeQuery = true)
    Integer allQuestionCount ();


    List<Question> findAllByOrderByDescription ();
}
