package ecma.demo.apptest.repository;

import ecma.demo.apptest.entity.Role;

import ecma.demo.apptest.entity.enums.RoleName;
import org.springframework.data.jpa.repository.JpaRepository;


import java.util.Set;
import java.util.UUID;

public interface RoleRepository extends JpaRepository<Role, UUID> {
    Set<Role> findAllByName(RoleName roleName);
}
