package ecma.demo.apptest.service;

import ecma.demo.apptest.entity.Answer;
import ecma.demo.apptest.entity.CheckAnswer;
import ecma.demo.apptest.entity.Question;
import ecma.demo.apptest.entity.User;
import ecma.demo.apptest.payload.ApiResponse;
import ecma.demo.apptest.payload.ReqQuestion;
import ecma.demo.apptest.repository.AnswerRepository;
import ecma.demo.apptest.repository.CheckAnswerRepository;
import ecma.demo.apptest.repository.QuestionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class QuestionService {

    @Autowired
    QuestionRepository questionRepository;
    @Autowired
    CheckAnswerRepository checkAnswerRepository;
    @Autowired
    AnswerRepository answerRepository;


    public HttpEntity<?> getAllQuestions(User user, Integer newPage) {
        if (checkAnswerRepository.findByUserId(user.getId()).isPresent()){
            Integer page = checkAnswerRepository.findByUserId(user.getId()).get().getPage();
            Pageable pageable= PageRequest.of(page-1,1);
            Page<Question> all = questionRepository.findAll(pageable);
            if (questionRepository.allQuestionCount()<page){
                Double totalMark = checkAnswerRepository.findByUserId(user.getId()).get().getTotalMark();
                CheckAnswer checkAnswer = checkAnswerRepository.findByUserId(user.getId()).get();
                checkAnswer.setTotalMark(0d);
                checkAnswer.setPage(1);
                checkAnswerRepository.save(checkAnswer);
                return ResponseEntity.ok(new ApiResponse("finished",true,totalMark));
            }
            return ResponseEntity.ok(new ApiResponse("success",true,all));
        }else {
            Pageable pageable= PageRequest.of(newPage-1,1);
            Page<Question> all = questionRepository.findAll(pageable);
            return ResponseEntity.ok(new ApiResponse("success",true,all));
        }
    }

    public HttpEntity<?> deleteQuestion(UUID id) {
        questionRepository.deleteById(id);
        return ResponseEntity.ok(new ApiResponse("success",true,null));
    }

    public HttpEntity<?> saveQuestion(ReqQuestion reqQuestion) {
        questionRepository.save(new Question(reqQuestion.getQuestion(),makeNewAnswersList(reqQuestion)));
        return ResponseEntity.ok(new ApiResponse("success",true,null));
    }

    public HttpEntity<?> getCurrentQuestion(UUID id) {
        Question question = questionRepository.findById(id).get();
        return ResponseEntity.ok(new ApiResponse("success",true,question));
    }

    public HttpEntity<?> updateQuestion(ReqQuestion reqQuestion) {
        questionRepository.deleteById(reqQuestion.getId());
        return saveQuestion(reqQuestion);
    }

    public List<Answer> makeNewAnswersList (ReqQuestion reqQuestion){
        List<Answer> answers=new ArrayList<>();
        Answer answer1 = answerRepository.save(new Answer(reqQuestion.getAnswer1(), reqQuestion.getAnswer().equals("a")? true:false));
        Answer answer2 = answerRepository.save(new Answer(reqQuestion.getAnswer2(), reqQuestion.getAnswer().equals("b")? true:false));
        Answer answer3 = answerRepository.save(new Answer(reqQuestion.getAnswer3(), reqQuestion.getAnswer().equals("c")? true:false));
        Answer answer4 = answerRepository.save(new Answer(reqQuestion.getAnswer4(), reqQuestion.getAnswer().equals("d")? true:false));
        answers.add(answer1);
        answers.add(answer2);
        answers.add(answer3);
        answers.add(answer4);
        return answers;
    }

}
