package ecma.demo.apptest.service;

import ecma.demo.apptest.entity.Role;
import ecma.demo.apptest.entity.User;
import ecma.demo.apptest.entity.enums.RoleName;
import ecma.demo.apptest.payload.ApiResponse;
import ecma.demo.apptest.payload.ReqUser;
import ecma.demo.apptest.repository.RoleRepository;
import ecma.demo.apptest.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;
    @Autowired
    PasswordEncoder passwordEncoder;
    @Autowired
    RoleRepository roleRepository;

    public HttpEntity<?> saveUser(ReqUser reqUser) {
        userRepository.save(new User(
                reqUser.getUserName(),
                passwordEncoder.encode(reqUser.getPassword()),
                reqUser.getFirstName(),
                reqUser.getLastName(),
                reqUser.getPatron(),
                reqUser.getPhoneNumber(),
                reqUser.getEmail(),
                roleRepository.findAllByName(RoleName.ROLE_USER)
        ));
        return ResponseEntity.ok(new ApiResponse("success",true,null));
    }

    public HttpEntity<?> getAllUser() {
        return ResponseEntity.ok(userRepository.findAll());
    }

    public HttpEntity<?> deleteUser(UUID id) {
        userRepository.deleteById(id);
        return ResponseEntity.ok(new ApiResponse("success",true,null));
    }

    public HttpEntity<?> updateUser(User user, ReqUser reqUser) {
        User editUser = userRepository.findById(user.getId()).get();
        editUser.setUserName(reqUser.getUserName());
        editUser.setPassword(passwordEncoder.encode(reqUser.getPassword()));
        editUser.setFirstName(reqUser.getFirstName());
        editUser.setLastName(reqUser.getLastName());
        editUser.setPatron(reqUser.getPatron());
        editUser.setPhoneNumber(reqUser.getPhoneNumber());
        editUser.setEmail(reqUser.getEmail());
        userRepository.save(editUser);
        return ResponseEntity.ok(new ApiResponse("success",true,null));
    }
}
